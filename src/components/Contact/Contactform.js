import React from 'react';
import { Formik } from 'formik';

/*
const validate = values => {
    const errors = {};
    if (!values.fullname){
        error.fullname = 'Required';
    }

    if(!values.email){
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid Email Address';
    }

    if (!values.message){
        errors.message = 'Required';
    }

    return errors;
};
*/

const ContactForm = () => {
    return (
    <Formik
    initialValues = {{fullname: '', email: '', message: '',}}
    validate = {(values) => {
        const errors = {};
    if (!values.fullname){
        error.fullname = 'Required';
    }

    if(!values.email){
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid Email Address';
    }

    if (!values.message){
        errors.message = 'Required';
    }

    return errors;}}
    onSubmit = {(values, {setSubmitting}) => {
        setTimeout(() => {
            alert(JSON.stringify(values,null,2));
            setSubmitting(false);
        }, 400);
    }}>
    {formik => (
        <form onSubmit= {formik.handleSubmit}>
        <label htmlFor="fullname">Name
        <input 
        id= "fullname"
        name = "fullname"
        type="text"
        onChange= {formik.handleChange}
        onBlur = {formik.handleBlur}
        value = {formik.values.fullname}
        /></label>
        {formik.touched.fullname && formik.errors.fullname ? (
            <div>{formik.errors.fullname}</div>
        ) : null }
        <label htmlFor="email">Email Address
        <input 
        id= "email"
        name = "email"
        type="email"
        onChange= {formik.handleChange}
        onBlur = {formik.handleBlur}
        value = {formik.values.email}
        />
        </label>
        {formik.touched.email && formik.errors.email ? (
            <div>{formik.errors.email}</div>
        ) : null }
        <label htmlFor="message">Message
        <textarea  cols="30" rows="10"
        id= "message"
        name = "message"
        type="text"
        onChange= {formik.handleChange}
        onBlur = {formik.handleBlur}
        value = {formik.values.message}>
        </textarea>
        </label>
        
        {formik.touched.message && formik.errors.message ? (
            <div>{formik.errors.message}</div>
        ) : null }
        <button type="submit">Reach Out</button>
    </form>
    )}
        

        </Formik>
    );
};

export default ContactForm;
