import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: 'Patience Ankunda | Junior Developer', // e.g: 'Name | Developer'
  lang: 'en', // e.g: en, es, fr, jp
  description: 'Simple Profile', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: 'Hey there! My name is',
  name: 'Patience Ankunda',
  subtitle: 'I am a learning Junior Developer',
  cta: 'See more',
};

// ABOUT DATA
export const aboutData = {
  img: 'wtm.jpeg',
  paragraphOne:
    'I am a "girls in tech" enthusiast, Architecture dropout, Computer Science student, squash player, physical health practioner, and female founder and entreprenuer of a healthy living startup.',
  paragraphTwo: '',
  paragraphThree: '',
  resume: 'https://www.resumemaker.online/es.php', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'java.png',
    title: 'Java',
    info:
      'This is my fav programming language and it is my intention to become a Senior Java developer someday. But for now, It is what it is.',
    info2: '',
    url: '',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'developerlogo.png',
    title: 'Community Development',
    info:
      "One of my recently discovered passions and now developing talent. Being able to impact a fellow developers' life through Developer Student Clubs at university has been a breath of fresh air.",
    info2: '',
    url: '',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'speaker.jpeg',
    title: 'Public Speaking',
    info:
      'I love talking and I have been able to transfer that love to speaking to audiences about what I am passionate about. I take each speaking event as an opportunity to improve myself and how I communicate my experiences.',
    info2: '',
    url: '',
    repo: '', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: 'Holla at me real quick',
  btn: 'Reach Out',
  email: 'patienceankundat@gmail.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'twitter',
      url: '',
    },
    {
      id: nanoid(),
      name: 'codepen',
      url: '',
    },
    {
      id: nanoid(),
      name: 'linkedin',
      url: '',
    },
    {
      id: nanoid(),
      name: 'github',
      url: '',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
